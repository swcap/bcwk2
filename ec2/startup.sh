#!/bin/bash

# set -ex
sudo apt update &> /tmp/startup.log
sudo apt-get update &>> /tmp/startup.log
# sudo apt-get install -y yum &>> /tmp/startup.log
# sudo yum update -y &&> /tmp/startup.log
sudo amazon-linux-extras install docker -y &>> /tmp/startup.log
sudo service docker start &>> /tmp/startup.log
sudo usermod -aG docker ${USER}
sudo docker pull svvatcap/pyserv &>> /tmp/startup.log
sudo docker run -p 8000:8000 -t svvatcap/pyserv &>> /tmp/startup.log