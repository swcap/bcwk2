#
Task - Create a Python Web application, deploy on Docker to AWS EC2 instance, with Terraform

## key-pair folder
Terraform code to create AWS Key Pair

##

###
Create EC2 instance in AWS with Key-Pair.

Key-Pair is not generated. It is simply referenced. Key-Pair will not have an associated pem file. This can be generated later.

### Create Key Pair in key-pair folder
  terraform apply -auto-approve

### Create EC2 instance
 
### Connect to EC2
  ssh -i "my-keypair.pem" ubuntu@ec2-18-169-163-142.eu-west-2.compute.amazonaws.com
### regenerate Key Pair in key-pair folder
  terraform destroy -auto-approve; terraform apply -auto-approve




## create key pair in Terraform
https://registry.terraform.io/modules/terraform-aws-modules/key-pair/aws/latest


After creating the instance & Key pair, the pem file will be created in the upper folder. 

Private & public keys must be exclude

Next the private & public keys must be deleted from the state.

terraform apply
terraform state rm tls_private_key.pk
terraform state rm aws_key_pair.kp

## end it
terraform destroy
