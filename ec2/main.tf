variable "key_name" {}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "eu-west-2"
}

data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_security_group" "allow_ssh" {
  name        = "linus_ports01"
  description = "Allow ports for linus"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_instance" "test" {
  # depends_on = ["aws_internet_gateway.test"]


  ami                         = data.aws_ami.amazon-linux-2.id
  associate_public_ip_address = true
  # iam_instance_profile        = aws_iam_instance_profile.test.id
  instance_type          = "t2.micro"
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  # subnet_id                   = aws_subnet.test.id

  // preinstall any updates or applications
  user_data = file("startup.sh")
  # user_data = <<-EOF
  #   #!/bin/bash
  #   set -ex
  #   sudo yum update -y
  #   sudo amazon-linux-extras install docker -y
  #   sudo service docker start > start.log
  #   sudo docker pull svvatcap/pyserv >> start.log
  #   sudo docker run -p 8000:8000 -t svvatcap/pyserv >> start.log
  # EOF

  # provisioner "remote-exec" {
  #   inline = [
  #     "docker pull svvatcap/pyserv",
  #     "docker run -p 8000:8000 -t svvatcap/pyserv"
  #   ]
  # }
}

