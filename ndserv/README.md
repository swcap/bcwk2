# Application description
A Typescript Web Service using Express, built as a Docker image, posted to dockerhub and hosted on AWS.


## install NodeJS

sudo apt install npm
npm install -g typescript

npm install express

### create tsconfig file
tsc --init

### linter
npm install eslint

### mocha
npm install mocha