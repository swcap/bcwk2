import express from 'express';

const app = express();

app.get('/', (req, res) => {
    res.send('Hello world from Typescript');
})

const server = app.listen(8001, () => {
    console.log('The application is listening on port 8001!');
})

function gracefulExit() {
  server.close(() => {
    console.log("HTTP Server closed");
    /*
     * Code for closing the DB connection and other resource
     * cleanup can be added here
     */
  })
}
process.on('SIGTERM', gracefulExit);
process.on('SIGINT', gracefulExit);