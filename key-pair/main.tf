variable "key_name" {}

terraform {
  required_providers {
    tls = {
      source = "hashicorp/tls"
    }
  }
}

provider "aws" {
  region = "eu-west-2"
}

# create private key
resource "tls_private_key" "pk" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# Output private key to pem file
resource "local_file" "pk-file" {
  content    = nonsensitive(tls_private_key.pk.private_key_pem)
  depends_on = [tls_private_key.pk, ]
  filename   = "${path.module}/../../${var.key_name}.pem"
}

# Create a keypair in AWS
resource "aws_key_pair" "kp" {
  key_name   = var.key_name
  public_key = tls_private_key.pk.public_key_openssh
  depends_on = [tls_private_key.pk, ]
}