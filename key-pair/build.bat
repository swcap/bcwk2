rem Recreate keypair
terraform destroy -auto-approve; terraform apply -auto-approve
terraform state rm tls_private_key.pk
terraform state rm aws_key_pair.kp
